import vehicles.Bicycle;
public class BikeStore{
    public static void main(String[] args){
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0] = new Bicycle("BMW", 5, 80);
        bicycles[1] = new Bicycle("YAMAHA", 9, 120);
        bicycles[2] = new Bicycle("Harley-Davidson", 7, 100);
        bicycles[3] = new Bicycle("MARKS", 10, 200);

        for(int i = 0; i < bicycles.length; i++){
            System.out.println(bicycles[i]);
        }
    }
}

//ANDREW MARKS 2237902